import {Navigation, LayoutRoot, Options} from 'react-native-navigation';
import Feeds from '@screens/Feeds';
import FeedItems from '@screens/FeedItems';
import Article from '@screens/Article';

export type {FeedItemsProps as FeedItemsScreenProps} from '@screens/FeedItems';
export type {ArticleProps as ArticleScreenProps} from '@screens/Article';

export enum Screens {
  feedsScreen = 'FeedsScreen',
  feedItemsScreen = 'FeedItems',
  articleScreen = 'Article',
}

Navigation.registerComponent(Screens.feedsScreen, () => Feeds);
Navigation.registerComponent(Screens.feedItemsScreen, () => FeedItems);
Navigation.registerComponent(Screens.articleScreen, () => Article);

export const mainNavFlow: LayoutRoot = {
  root: {
    stack: {
      children: [
        {
          component: {
            name: Screens.feedsScreen,
          },
        },
      ],
    },
  },
};

export const defaultNavOptions: Options = {
  statusBar: {
    backgroundColor: '#165C7D',
  },
  topBar: {
    title: {
      color: 'white',
    },
    backButton: {
      color: 'white',
    },
    background: {
      color: '#165C7D',
    },
  },
};

export default mainNavFlow;
