import rssParser, {Feed as IFeed} from 'react-native-rss-parser';
import {showMessage} from 'react-native-flash-message';
import {DateTime} from 'luxon';

export type {
  Feed as IFeed,
  FeedItem as IFeedItem,
} from 'react-native-rss-parser';

export interface IFeedListItem {
  name: string;
  source: string;
}

export interface IFeedListItem {
  name: string;
  source: string;
}

const handleApiError = (e: Error, userMessage?: string) => {
  console.log(`API Error: ${e.message}`);
  showMessage({
    message: userMessage === undefined ? e.message : userMessage,
    type: 'danger',
  });
};

export const getFeeds = async (): Promise<IFeedListItem[] | null> => {
  try {
    //network simulation
    await Promise.resolve(new Promise(resolve => setTimeout(resolve, 1000)));
    return [
      {
        name: 'Hacker News',
        source: 'https://news.ycombinator.com/rss',
      },
      {
        name: 'DW',
        source: 'https://rss.dw.com/rdf/rss-en-ger',
      },
      {
        name: 'Reddit',
        source: 'https://www.reddit.com/.rss',
      },
      {
        name: 'The Guardian',
        source: 'https://www.theguardian.com/world/rss',
      },
      {
        name: 'NASA',
        source: 'http://www.nasa.gov/rss/dyn/breaking_news.rss',
      },
    ];
  } catch (e) {
    handleApiError(e, 'Unable to load feed list. Try later');
    return null;
  }
};

export const getFeedDetails = async (source: string): Promise<IFeed | null> => {
  try {
    const feedDetails = fetch(source)
      .then(response => response.text())
      .then(responseData => rssParser.parse(responseData))
      .then(rss => feedMutator(rss));

    return feedDetails;
  } catch (e) {
    handleApiError(e, 'Unable to load feed details. Try later');
    return null;
  }
};

const feedMutator = (feed: IFeed): IFeed => {
  feed.items = feed.items
    .map(item => {
      let published = item.published;
      if (DateTime.fromISO(published).isValid) {
        published = DateTime.fromISO(published).toISO();
      } else if (DateTime.fromHTTP(published).isValid) {
        published = DateTime.fromHTTP(published).toISO();
      } else if (DateTime.fromRFC2822(published).isValid) {
        published = DateTime.fromRFC2822(published).toISO();
      }
      return {...item, published};
    })
    .sort((a, b) => {
      return DateTime.fromISO(a.published) > DateTime.fromISO(b.published)
        ? -1
        : 1;
    });
  return feed;
};
