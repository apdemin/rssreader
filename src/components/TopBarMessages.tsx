import React from 'react';
import {StyleSheet, View} from 'react-native';
import FlashMessage from 'react-native-flash-message';

const styles = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
  },
});

const TopBarMessages = () => {
  return (
    <View style={styles.container}>
      <FlashMessage position="top" />
    </View>
  );
};

export default TopBarMessages;
