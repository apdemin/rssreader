import React from 'react';
import WebView from 'react-native-webview';

export interface ArticleProps {
  source: string;
}

const Article: React.FC<ArticleProps> = ({source}) => {
  return <WebView source={{uri: source}} />;
};

export default Article;
