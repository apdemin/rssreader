import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import {NavigationFunctionComponent, Navigation} from 'react-native-navigation';
import {Screens, FeedItemsScreenProps} from '@navigation';
import {IFeedListItem, getFeeds} from '@api';

const Item: React.FC<{item: IFeedListItem; componentId: string}> = ({
  item,
  componentId,
}) => (
  <TouchableOpacity
    style={styles.item}
    onPress={() => {
      Navigation.push<FeedItemsScreenProps>(componentId, {
        component: {
          name: Screens.feedItemsScreen,
          passProps: {
            feed: item,
          },
        },
      });
    }}>
    <Text style={styles.title}>{item.name}</Text>
  </TouchableOpacity>
);

const Feeds: NavigationFunctionComponent = ({componentId}) => {
  const renderItem = ({item}: {item: IFeedListItem}) => (
    <Item item={item} componentId={componentId} />
  );
  const [feedList, setFeedList] = useState<IFeedListItem[]>([]);
  const [loading, setLoadingStatus] = useState(true);

  const fetchData = async () => {
    setLoadingStatus(true);
    const data = await getFeeds();
    data && setFeedList(data);
    setLoadingStatus(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        refreshing={loading}
        onRefresh={fetchData}
        data={feedList}
        renderItem={renderItem}
        keyExtractor={item => item.source}
      />
    </SafeAreaView>
  );
};

Feeds.options = {
  topBar: {
    title: {
      text: 'Feeds',
      alignment: 'center',
    },
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#29a4dd',
    padding: 20,
    marginBottom: 2,
  },
  title: {
    fontSize: 32,
  },
});

export default Feeds;
