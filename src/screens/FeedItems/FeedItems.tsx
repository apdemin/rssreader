import React, {useState, useEffect} from 'react';
import {DateTime} from 'luxon';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import {NavigationFunctionComponent, Navigation} from 'react-native-navigation';
import {IFeedListItem, IFeed, IFeedItem, getFeedDetails} from '@api';
import {Screens, ArticleScreenProps} from '@navigation';

export interface FeedItemsProps {
  feed: IFeedListItem;
}

const Item: React.FC<{item: IFeedItem; componentId: string}> = ({
  componentId,
  item: {title, description, published, links},
}) => (
  <TouchableOpacity
    style={styles.item}
    onPress={() => {
      Navigation.push<ArticleScreenProps>(componentId, {
        component: {
          name: Screens.articleScreen,
          passProps: {
            source: links[0].url,
          },
        },
      });
    }}>
    <Text style={styles.title}>{title}</Text>
    {description && (
      <Text style={styles.description}>
        {description.replace(/(<([^>]+)>)/gi, '')}
      </Text>
    )}
    <Text style={styles.published}>
      {DateTime.fromISO(published).isValid
        ? DateTime.fromISO(published).toLocaleString(DateTime.DATETIME_MED)
        : published}
    </Text>
  </TouchableOpacity>
);

const FeedItems: NavigationFunctionComponent<FeedItemsProps> = ({
  feed,
  componentId,
}) => {
  const renderItem = ({item}: {item: IFeedItem}) => (
    <Item item={item} componentId={componentId} />
  );
  const [feedData, setFeedData] = useState<IFeed | null>(null);
  const [loading, setLoadingStatus] = useState(true);

  const fetchData = async () => {
    setLoadingStatus(true);
    const data = await getFeedDetails(feed.source);
    data && setFeedData(data);
    setLoadingStatus(false);
  };

  useEffect(() => {
    fetchData();
  }, [feed]);

  useEffect(() => {
    Navigation.mergeOptions(componentId, {
      topBar: {
        title: {
          text: `${feedData ? feedData.title : feed.name} (${
            feedData?.items.length || 0
          })`,
          alignment: 'fill',
        },
      },
    });
  }, [feedData, componentId, feed]);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        refreshing={loading}
        onRefresh={fetchData}
        data={feedData ? feedData.items : null}
        renderItem={renderItem}
        keyExtractor={({id, title, published}) => id || `${title}-${published}`}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#29a4dd',
    padding: 20,
    marginBottom: 2,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 20,
    paddingTop: 4,
  },
  published: {
    fontSize: 18,
    paddingTop: 4,
    textAlign: 'right',
  },
});

export default FeedItems;
