import {Navigation} from 'react-native-navigation';
import {mainNavFlow, defaultNavOptions} from '@navigation';
import TopBarMessages from '@components/TopBarMessages';

Navigation.registerComponent('topBarMessages', () => TopBarMessages);
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot(mainNavFlow);
});

Navigation.setDefaultOptions(defaultNavOptions);

Navigation.showOverlay({
  component: {
    name: 'topBarMessages',
    passProps: {},
    options: {
      overlay: {
        interceptTouchOutside: false,
      },
      layout: {
        backgroundColor: 'transparent',
        orientation: ['portrait'],
      },
    },
  },
});
